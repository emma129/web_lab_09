/**
 * Created by yzhb363 on 18/04/2017.
 */

var myVar = setInterval(myTimer ,1000);

function myTimer() {
    var d = new Date();
    document.getElementById("time").innerHTML = d.toLocaleTimeString();
}

function stop() {
    clearInterval(myVar);

}

function resume() {
    myVar = setInterval(myTimer,1000);
}
